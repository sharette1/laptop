Laptop
======

Download, review, then execute the script:

    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/sharette/laptop/master/start.rb)"
